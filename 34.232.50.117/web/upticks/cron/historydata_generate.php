<?php
	include('../src/constant.php');
	$con = mysqli_connect(DBHOST,DBUSER,DBPASSWORD);
	if(!mysqli_select_db($con,DBNAME)){
		echo 'Error in DB connection';exit;
	}
	//die;
	$dtd 	= '2018-01-16';
	$code 	= 0;
	$sql 	= 'select * from ctd';
	$res	= mysqli_query($con,$sql);
	while($row = mysqli_fetch_assoc($res))
	{
		$dtd	= $row['ctd_date'];
		$code	= $row['code'];
	}
	
	if($dtd>date('Y-m-d')){
		echo 'All History Data Allready Updated';die;
	}
	
	
	if(!$code){
		$x 		= 12;
		$y 		= 11;
	}
	else{
		$x 		= 11;
		$y 		= 12;
	}
	
	$dtd1 				= date('Ymd',strtotime($dtd));
	$ct_id				= '';
	$name 				= '';
	$ct_name_symbol 	= '';
	$rank 				= 0;
	$updated_time 		= date('Y-m-d H:i:s');	
	$price_btc 			= 1;//** need to calculate usd=>btc on that date
	$percent_change_24h = 0;
	$percent_change_7d	= 0;
	
	
	if(1)
	{
		$sql = "CREATE TABLE IF NOT EXISTS `coins_tokens_data_".$dtd1."` (
  `ids` int(11) NOT NULL AUTO_INCREMENT,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ct_name_symbol` varchar(50) DEFAULT NULL,
  `ct_id` varchar(125) NOT NULL,
  `name` varchar(125) NOT NULL,
  `symbol` varchar(125) NOT NULL,
  `rank` int(11) NOT NULL DEFAULT '9999',
  `price_usd` double NOT NULL DEFAULT '0',
  `price_btc` double NOT NULL DEFAULT '0',
  `24h_volume_usd` double NOT NULL DEFAULT '0',
  `market_cap_usd` double DEFAULT '0',
  `available_supply` double NOT NULL DEFAULT '0',
  `total_supply` double NOT NULL DEFAULT '0',
  `max_supply` double NOT NULL DEFAULT '0',
  `percent_change_1h` double NOT NULL DEFAULT '0',
  `percent_change_24h` double NOT NULL,
  `percent_change_7d` double NOT NULL DEFAULT '0',
  `last_updated` varchar(125) DEFAULT NULL,
  `workinprogress` tinyint(4) NOT NULL DEFAULT '0',
  `type` enum('coin','token') NOT NULL,
  `market` varchar(125) DEFAULT NULL,
  `volatility_ratio_10_days` double DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `market_time` int(11) NOT NULL DEFAULT '0',
  `growth_time` int(11) NOT NULL DEFAULT '0',
  `seourl` varchar(100) NOT NULL DEFAULT 'seourl',
  `iconcollected` tinyint(4) NOT NULL DEFAULT '9',
  PRIMARY KEY (`ids`),
  UNIQUE KEY `ct_id` (`ct_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";
//mysqli_query($con,$sql);
$sql = "
CREATE TABLE `growth_indexes_".$dtd1."` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ct_name_symbol` varchar(50) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `market_cap_usd` double NOT NULL DEFAULT '0',
  `volatility_ratio_10_days` double DEFAULT NULL,
  `atd` double NOT NULL DEFAULT '0',
  `ytd` double NOT NULL DEFAULT '0',
  `ath_price` double NOT NULL DEFAULT '0',
  `atl_price` double NOT NULL DEFAULT '0',
  `ath_price_date` date NOT NULL,
  `atl_price_date` date NOT NULL,
  `minprice_7days` double NOT NULL DEFAULT '0',
  `maxprice_7days` double NOT NULL DEFAULT '0',
  `minprice_1months` double NOT NULL DEFAULT '0',
  `maxprice_1months` double NOT NULL DEFAULT '0',
  `sevenDays_avg_price` double NOT NULL DEFAULT '0',
  `sevenDays_avg_volume` double NOT NULL DEFAULT '0',
  `30days_avg_price` double NOT NULL DEFAULT '0',
  `30days_avg_volume` double NOT NULL DEFAULT '0',
  `gi_week_1` double DEFAULT NULL,
  `gi_week_2` double DEFAULT NULL,
  `gi_week_3` double DEFAULT NULL,
  `gi_week_4` double DEFAULT NULL,
  `gi_week_5` double DEFAULT NULL,
  `gi_week_6` double DEFAULT NULL,
  `gi_week_7` double DEFAULT NULL,
  `gi_week_8` double DEFAULT NULL,
  `gi_week_9` double DEFAULT NULL,
  `gi_week_10` double DEFAULT NULL,
  `gi_week_11` double DEFAULT NULL,
  `gi_week_12` double DEFAULT NULL,
  `gi_month_1` double DEFAULT NULL,
  `gi_month_2` double DEFAULT NULL,
  `gi_month_3` double DEFAULT NULL,
  `gi_month_4` double DEFAULT NULL,
  `gi_month_5` double DEFAULT NULL,
  `gi_month_6` double DEFAULT NULL,
  `gi_month_7` double DEFAULT NULL,
  `gi_month_8` double DEFAULT NULL,
  `gi_month_9` double DEFAULT NULL,
  `gi_month_10` double DEFAULT NULL,
  `gi_month_11` double DEFAULT NULL,
  `gi_month_12` double DEFAULT NULL,
  `gi_uarter_1` double DEFAULT NULL,
  `gi_uarter_2` double DEFAULT NULL,
  `gi_uarter_3` double DEFAULT NULL,
  `gi_uarter_4` double DEFAULT NULL,
  `listingprice_ytd` double NOT NULL DEFAULT '0',
  `listingprice` double DEFAULT NULL,
  `listingdate` date NOT NULL DEFAULT '2010-01-01',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";
//mysqli_query($con,$sql);
	}
	
	if(0)
	{
		$sql = "truncate table `coins_tokens_data_".$dtd1."`";
		//mysqli_query($con,$sql);
		
		$sql = "truncate table `growth_indexes_".$dtd1."`";
		//mysqli_query($con,$sql);
	}
	
	
	$sql 			= "select *  from `coins_tokens_data` where name != '' and ct_name_symbol != '' and iconcollected = ".$x." and rank < 7  order by ids limit 1";
	$result 		= mysqli_query($con,$sql);
	while($row = mysqli_fetch_assoc($result))
	{
		$ids	= $row['ids'];
		$ct_id 	= $row['ct_id'];
		$name 	= $row['name'];
		$symbol = $row['symbol'];
		$type	= $row['type'];
		$rank	= $row['rank'];
		$seourl	= $row['seourl'];
		$ct_name_symbol = $row['ct_name_symbol'];
		
		$sql 	= "UPDATE `coins_tokens_data` SET iconcollected = ".$y." WHERE ids = ".$ids;
		mysqli_query($con,$sql);
		//echo '<pre>';print_r($row);echo '</pre>';		
	}
	
	if($ct_id == ''){
		if($code){
			$code = 0;			
		}else{
			$code = 1;
		}
		$sql = "update ctd set ctd_date = '".date('Y-m-d',strtotime('+1 days',strtotime($dtd)))."', code = ".$code;
		mysqli_query($con,$sql);
		echo 'Coin not selected properly.';exit;
	}

	$sql 	= "select open, high, low, close, volum, market_cap from `historical_data` where name = '".$ct_id."' and date = '".$dtd."'";
	$result = mysqli_query($con,$sql);
	while($row = mysqli_fetch_assoc($result))
	{
		$open 			= $row['open'];
		$high 			= $row['high'];
		$low 			= $row['low'];
		$price_usd 		= $close 		= $row['close'];
		$market_cap_usd = $market_cap 	= $row['market_cap'];
		$twentyfourhr_volume_usd		= $volum 	= $row['volum'];
	}


	$sql 	= "select open, high, low, close, volum, market_cap from `historical_data` where name = '".$ct_id."' and close > 0 and date = '".date('Y-m-d',strtotime('-1 day', strtotime($dtd)))."'";
	$result = mysqli_query($con,$sql);
	while($row = mysqli_fetch_assoc($result))
	{
		$percent_change_24h = (100*($price_usd - $row['close']))/$row['close'];
	}

	$sql 	= "select open, high, low, close, volum, market_cap from `historical_data` where name = '".$ct_id."' and close > 0 and date = '".date('Y-m-d',strtotime('-7 day', strtotime($dtd)))."'";
	$result = mysqli_query($con,$sql);
	while($row = mysqli_fetch_assoc($result))
	{
		$percent_change_7d = (100*($price_usd - $row['close']))/$row['close'];
	}

	
	$sql = "
		INSERT INTO	`coins_tokens_data_".$dtd1."` SET 
		ids 				=  ".$ids.", 
		ct_name_symbol		= '".$ct_name_symbol."',
		ct_id				= '".$ct_id."',
		name				= '".$name."',
		symbol				= '".$symbol."',
		rank				= ".$rank.",
		price_usd			= ".$price_usd.",
		price_btc			= ".$price_btc.",
		24h_volume_usd		= ".$twentyfourhr_volume_usd.",
		market_cap_usd 		= ".$market_cap_usd.",
		percent_change_24h	= '".$percent_change_24h."',
		percent_change_7d	= '".$percent_change_7d."',
		type				= '".$type."', 
		seourl				= '".$seourl."'";
	
	//echo $sql.'<br><br>';
	//mysqli_query($con,$sql);
	

	

$volatility_ratio_10_days = 0;
$atd 		= 0;
$ytd 		= 0;
$ath_price 	= 0;
$atl_price 	= 0;
$ath_price_date 	= 0;
$atl_price_date 	= 0;
$minprice_7days 	= 0;
$maxprice_7days 	= 0;
$minprice_1months 	= 0;
$maxprice_1months 	= 0;
$sevenDays_avg_price = 0;
$sevenDays_avg_volume = 0;
$thirtydays_avg_price = 0;
$thirtydays_avg_volume = 0;
$gi_week_1 = 0;
$gi_week_2 = 0;
$gi_week_3 = 0;
$gi_week_4 = 0;
$gi_week_5 = 0;
$gi_week_6 = 0;
$gi_week_7 = 0;
$gi_week_8 = 0;
$gi_week_9 = 0;
$gi_week_10 = 0;
$gi_week_11 = 0;
$gi_week_12 = 0;
$gi_month_1 = 0;
$gi_month_2 = 0;
$gi_month_3 = 0;
$gi_month_4 = 0;
$gi_month_5 = 0;
$gi_month_6 = 0;
$gi_month_7 = 0;
$gi_month_8 = 0;
$gi_month_9 = 0;
$gi_month_10 = 0;
$gi_month_11 = 0;
$gi_month_12 = 0;
$gi_uarter_1 = 0;
$gi_uarter_2 = 0;
$gi_uarter_3 = 0;
$gi_uarter_4 = 0;


//$atd_price_index	= (($current_price-$atd_price)/$atd_price)*100;
if(1)
{
	
	
$sql_ytd 	= "SELECT name,open,date FROM historical_data where name = '".$name."' and date >= '".date('Y-01-01')."' and date <= '".date('Y-m-d',strtotime($dtd))."' order by date asc limit 1 ";
$run_ytd 	= mysqli_query($con,$sql_ytd);
foreach($run_ytd as $row) 
{
	$ytd	= (100*($price_usd - $row['open']))/$row['open'];
}

$sql_atd 	= "SELECT name,open,date FROM historical_data where name = '".$name."' and date <= '".date('Y-m-d',strtotime($dtd))."' order by date asc limit 1 ";
$run_atd 	= mysqli_query($con,$sql_atd);
foreach($run_atd as $row) 
{
	$atd	= (100*($price_usd - $row['open']))/$row['open'];
}
	
$i = 0;
$wsql = "";
while($i<12)
{
	$i++;
	$gi_week[$i] = 0;
	$days 	= 7*$i;
	$sql 	= "select open, high, low, close, volum, market_cap from `historical_data` where name = '".$ct_id."' and close > 0 and date = '".date('Y-m-d',strtotime('-'.$days.' day', strtotime($dtd)))."'";
	$result = mysqli_query($con,$sql);
	while($row = mysqli_fetch_assoc($result))
	{
		$gi_week[$i] = (100*($price_usd - $row['close']))/$row['close'];
	}
	$wsql .= "gi_week_".$i." = '".$gi_week[$i]."',";
}


$i = 0;
$msql = "";
while($i<12)
{
	$i++;
	$gi_month[$i] = 0;
	$days = 30*$i;
	$sql 	= "select open, high, low, close, volum, market_cap from `historical_data` where name = '".$ct_id."' and close > 0 and date = '".date('Y-m-d',strtotime('-'.$days.' day', strtotime($dtd)))."'";
	$result = mysqli_query($con,$sql);
	while($row = mysqli_fetch_assoc($result))
	{
		$gi_month[$i] = (100*($price_usd - $row['close']))/$row['close'];
	}
	$msql .= "gi_month_".$i." = '".$gi_month[$i]."',";
}

$i = 0;
$qsql = "";
while($i<4)
{
	$i++;
	$gi_uarter[$i] = 0;
	$days 	= 120*$i;
	$sql 	= "select open, high, low, close, volum, market_cap from `historical_data` where name = '".$ct_id."' and close > 0 and date = '".date('Y-m-d',strtotime('-'.$days.' day', strtotime($dtd)))."'";
	$result = mysqli_query($con,$sql);
	while($row = mysqli_fetch_assoc($result))
	{
		$gi_quarter[$i] = (100*($price_usd - $row['close']))/$row['close'];
	}
	$qsql .= "gi_uarter_".$i." = '".$gi_quarter[$i]."',";
}
$qsql = substr($qsql,0,-1);

}






		/***Start : Min-Max 7 days, 1 month ***/
		$ath_price = 0;
		$atl_price = 0;
		
		$sql 	= "select date,high from `historical_data` where name = '".$name."' and high = ( select max(high)  from `historical_data` where name = '".$name."'  and date <= '".date('Y-m-d',strtotime($dtd))."'  )";
		$run 	= mysqli_query($con,$sql);
		foreach($run as $val) 
		{
			$ath_price		= $val['high'];
			$ath_price_date	= $val['date'];
		}
		
		$sql 	= "select date,low from `historical_data` where name = '".$name."' and low = ( select min(low)  from `historical_data` where name = '".$name."'  and date <= '".date('Y-m-d',strtotime($dtd))."' )";
		$run 	= mysqli_query($con,$sql);
		foreach($run as $val) 
		{
			$atl_price		= $val['low'];
			$atl_price_date	= $val['date'];
		}
		
		$minprice_7days		= 0;
		$maxprice_7days		= 0;
		$sql 	= "select min(low) minprice, max(high) maxprice from `historical_data` where date > '".date('Y-m-d',strtotime('-7 day', strtotime($dtd)))."' and name = '".$name."'  and date <= '".date('Y-m-d',strtotime($dtd))."' order by date desc";
		
		$run 	= mysqli_query($con,$sql);
		foreach($run as $val) 
		{
			$minprice_7days		= $val['minprice'];
			$maxprice_7days		= $val['maxprice'];
		}
		
		$minprice_1months		= 0;
		$maxprice_1months		= 0;
		$sql 	= "select min(low) minprice, max(high) maxprice from `historical_data` where date > '".date('Y-m-d',strtotime('-1 months', strtotime($dtd)))."' and name = '".$name."' and date <= '".date('Y-m-d',strtotime($dtd))."'  order by date desc";
		$run 	= mysqli_query($con,$sql);
		foreach($run as $val) 
		{
			$minprice_1months		= $val['minprice'];
			$maxprice_1months		= $val['maxprice'];
		}
		
		/***End : Min-Max 7 days, 1 month  ***/
























$sql = "INSERT INTO `growth_indexes_".$dtd1."`  SET 
id = ".$ids.",
ct_name_symbol = '".$ct_name_symbol."',
name = '".$ct_id."',
market_cap_usd = ".$market_cap_usd.",

volatility_ratio_10_days = ".$volatility_ratio_10_days.",
atd 		= '".$atd."',
ytd 		= '".$ytd."',

ath_price 		= ".$ath_price.",
atl_price 		= ".$atl_price.",
ath_price_date 	= '".$ath_price_date."',
atl_price_date 	= '".$atl_price_date."',

minprice_7days 			= ".$minprice_7days.",
maxprice_7days 			= ".$maxprice_7days.",
minprice_1months 		= ".$minprice_1months.",
maxprice_1months 		= ".$maxprice_1months.",

sevenDays_avg_price 	= ".$sevenDays_avg_price.",
sevenDays_avg_volume 	= ".$sevenDays_avg_volume.",
30days_avg_price 		= ".$thirtydays_avg_price.",
30days_avg_volume 		= ".$thirtydays_avg_volume.",";

$sql .= $wsql;
$sql .= $msql;
$sql .= $qsql;

//echo $sql;
//mysqli_query($con,$sql);

echo $dtd.'...'.$ct_id.'...
';

header('Location:https://upticks.io/cron/historydata_generate.php');
	
?>