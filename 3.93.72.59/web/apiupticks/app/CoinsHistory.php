<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoinsHistory extends Model
{
    protected $table = 'coins_data_history';
}
