<?php //echo '<pre>';print_r($mydata);echo '</pre>';?>

<!DOCTYPE html>
<html>
<head>
<title>Exchange List</title>
<link rel="shortcut icon" type="image/png" href="api.png" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script></head>
<script>
$(document).ready(function() {
    $('#exchange_list').DataTable({"pageLength": 10});
} );
</script>
<body>
<h3><?php echo apimenu();?>
Show Coin Pair Price</h3>


<table id="exchange_list" class="display" style="width:100%">
	<thead>
	<tr>
		<td>id</td>
		<td>Currency</td>
		<td style="text-align:right;">Price BTC</td>
		<td style="text-align:right;">Price USD</td>
		<td style="text-align:right;">Price KRW</td>
		<td>&nbsp; &nbsp; </td>
	</tr>
	</thead>
	<tbody>
		<?php
			$i = 0;
			foreach($mydata['coinprices'] as $key=>$val)
			{
				$i++;
				echo '<tr><td>'.$i.'</td>';
				echo '<td>'.$key.' to BTC</td>';
				echo '<td style="text-align:right;">'.number_format($val,8).'</td>';
				echo '<td style="text-align:right;">'.number_format($val*$mydata['btc_usd'],8).'</td>';
				echo '<td style="text-align:right;">'.number_format($val*$mydata['btc_krw']).'</td>';
				echo '<td>&nbsp; &nbsp; </td>';
				echo '</tr>';
			}
		?>	
	<tbody>
</table>


</body>
</html>

