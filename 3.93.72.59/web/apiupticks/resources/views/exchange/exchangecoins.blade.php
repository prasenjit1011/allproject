<!DOCTYPE html>
<html>
<head>
<title>Exchange List</title>
<link rel="shortcut icon" type="image/png" href="api.png" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script></head>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script>
$(document).ready(function() {
    $('#exchange_list').DataTable({
		"pageLength": 100,
		fixedHeader: {
            header: true,
            footer: true
        }
	});
} );
</script>
<body>

<h3><?php echo apimenu().'Exchange : '.$data['exchange'][0]->exchange_name;?><span id="total_volume"></span></h3>
<div>
<h3>
<a target="_blank" style="color:#009" href="http://api.upticks.io/coindata/<?php echo $data['exchange'][0]->id;?>">Update Exchange Data</a> &nbsp; &nbsp;
<a style="color:#009" href="http://api.upticks.io/exchange/details/<?php echo $data['exchange'][0]->id;?>">Show All Data</a> &nbsp; &nbsp; &nbsp;
<a style="color:#009" href="http://api.upticks.io/exchange/details/<?php echo $data['exchange'][0]->id;?>?showbtc=btc">Show BTC Price Only</a> &nbsp; &nbsp; &nbsp;
<a style="color:#009" href="http://api.upticks.io/exchange/details/<?php echo $data['exchange'][0]->id;?>?showbtc=usd">Show USD Price Only</a> &nbsp; &nbsp; &nbsp;
<a style="color:#009" href="http://api.upticks.io/exchange/coins/<?php echo $data['exchange'][0]->id;?>">Coins BTC Volume</a>
</h3>
<br>
</div>

<table id="exchange_list" class="display" style="width:100%">
	<thead>
	<tr>
		<td>sl</td>
		<td>id</td>
		<td>Symbol</td>
		<td style="text-align:right; padding-right:20px;">Volume BTC</td>
	</tr>
	</thead>
	<tbody>
		<?php
			$sl = 0;
			foreach($data['exchangecoins'] as $val)
			{
				$sl++;
				echo '<tr><td>'.$sl.'</td><td>'.$val->id.'</td><td>'.$val->symbol.'</td><td style="text-align:right; padding-right:20px;">'.number_format($val->volume_btc,4).'</td></tr>';
			}
		?>	
	</tbody>
</table>



</body>
</html>
