<!DOCTYPE html>
<html>
<head>
<title>Exchange List</title>
<link rel="shortcut icon" type="image/png" href="api.png" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script></head>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script>
$(document).ready(function() {
    $('#exchange_list').DataTable({
		"pageLength": 10,
		fixedHeader: {
            header: true,
            footer: true
        }
	});
} );
</script>
<body>
<h1>Show Coin Price</h1><?php echo apimenu();?>
<table id="exchange_list" class="display" style="width:100%">
	<thead>
	<tr>
		<td>id</td>
		<td>Sym1</td>
		<td style="text-align:right;">Price BTC</td>		
		<td style="text-align:right;">Price USD</td>		
		<td style="text-align:right;">Price KRW</td>
		<td style="text-align:right;">Volume BTC</td>		
		<td style="text-align:right;">Volume USD</td>		
		<td style="text-align:right;">Volume KRW</td>
	</tr>
	</thead>
	<tbody>
		<?php
			$i = 0;
			foreach($data['coinprices'] as $k=>$val)
			{
				$i++;
				echo '<tr><td>'.$i.'</td><td>'.$k.'</td>
				<td style="text-align:right;">'.number_format($val['price_btc'],18).' BTC</td>
				<td style="text-align:right;">'.number_format($val['price_btc']*$data['btc_usd'],4).' USD</td>
				<td style="text-align:right;">'.number_format($val['price_btc']*$data['btc_krw'],0).' KRW</td>
				<td style="text-align:right;">'.number_format($val['volume_btc'],0).' BTC</td>
				<td style="text-align:right;">'.number_format($val['volume_btc']*$data['btc_usd'],4).' USD</td>
				<td style="text-align:right;">'.number_format($val['volume_btc']*$data['btc_krw'],0).' KRW</td>
				</tr>';
			}
		?>	
	<tbody>
</table>


</body>
</html>

