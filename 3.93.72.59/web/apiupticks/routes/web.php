<?php
Route::get('/', ['as' => 'exchangelist', 'uses' => 'ExchangeController@exchangelist']);
Route::get('/login', ['as' => 'exchangelist', 'uses' => 'ExchangeController@login']);
Route::post('/login', ['as' => 'exchangelist', 'uses' => 'ExchangeController@login']);
Route::get('logout', ['as' => 'logout', 'uses' => 'ExchangeController@logout']);
Route::get('api', ['as' => 'api', 'uses' => 'ExchangeController@api']);


Route::get('/coin/exchange/volume/{symbol}', ['as' => 'coinexchangevolume', 'uses' => 'ExchangeController@coinexchangevolume']);
Route::get('/coincirculating', ['as' => 'coincirculating', 'uses' => 'ExchangeController@coincirculating']);
Route::get('/coin/exchange/price/{symbol}', ['as' => 'coinexchangeprice', 'uses' => 'ExchangeController@coinexchangeprice']);
Route::get('/market/active/{market_id}', ['as' => 'marketactive', 'uses' => 'ExchangeController@marketactive']);
Route::get('/market/inactive/{market_id}', ['as' => 'marketinactive', 'uses' => 'ExchangeController@marketinactive']);
Route::get('/exchangeexport/{exchange_id}', ['as' => 'exchangeexport', 'uses' => 'CoindataController@exchangeexport']);
Route::get('/exchange_export/{exchange_id}', ['as' => 'exchange_export', 'uses' => 'CoindataController@exchange_export']);
Route::get('/exchangeimport/{exchange_id}', ['as' => 'exchangeimport', 'uses' => 'CoindataController@exchangeimport']);

Route::get('/exchange/marketlist', ['as' => 'exchangemarketlist', 'uses' => 'ExchangeController@exchangemarketlist']);
Route::get('/marketpair/details/{marketname}', ['as' => 'marketpairdetails', 'uses' => 'ExchangeController@marketpairdetails']);
Route::get('/coinmarketcap/exchangerank', ['as' => 'coinmarketcapexchangerank', 'uses' => 'ExchangeController@coinmarketcapexchangerank']);
Route::get('/exchangevolume', ['as' => 'exchangevolume', 'uses' => 'ExchangeController@exchangevolume']);

Route::get('/setzero/{exchange_id}', ['as' => 'setzero', 'uses' => 'CoindataController@setzero']);
Route::get('/setzeroall', ['as' => 'setzeroall', 'uses' => 'CoindataController@setzeroall']);
Route::get('/exchange/active/{exchange_id}', ['as' => 'exchangeactive', 'uses' => 'CoindataController@exchangeactive']);
Route::get('/exchangeinactivemarketlist', ['as' => 'exchangeinactivemarketlist', 'uses' => 'ExchangeController@exchangeinactivemarketlist']);
Route::get('/apicron', ['as' => 'apicron', 'uses' => 'ExchangeController@apicron']);

Route::get('/coin_exchangeprice/status/{id}/{status}', ['as' => 'coin_exchangeprice_status', 'uses' => 'ExchangeController@coin_exchangeprice_status']);

Route::get('/coinhistorypricelist/{symbol}', ['as' => 'coinhistorypricelist', 'uses' => 'ExchangeController@coinhistorypricelist']);


Route::get('/coinhistoryprice', ['as' => 'coinhistoryprice', 'uses' => 'ExchangeController@coinhistoryprice']);
Route::get('/coinhistorypricenew', ['as' => 'coinhistorypricenew', 'uses' => 'ExchangeController@coinhistorypricenew']);
Route::get('/coinhistoryprice_new', ['as' => 'coinhistoryprice_new', 'uses' => 'ExchangeController@coinhistoryprice_new']);


Route::get('/calculateavg/', ['as' => 'calculateavg', 'uses' => 'ExchangeController@calculateavg']);
Route::get('/calculateavg/{symbol}', ['as' => 'calculateavg_coin', 'uses' => 'ExchangeController@calculateavg']);
Route::get('/coins/total/volume', ['as' => 'coinstotalvolume', 'uses' => 'ExchangeController@coinstotalvolume']);
Route::get('exchange/currencypair', ['as' => 'currencypair', 'uses' => 'ExchangeController@currencypair']);
Route::get('exchange/coins/{exchange_id}', ['as' => 'exchangecoins', 'uses' => 'ExchangeController@exchangecoins']);
Route::get('exchange/details', ['as' => 'exchagesalldata', 'uses' => 'ExchangeController@exchagesalldata']);
Route::get('exchange/details/{exchange_id}', ['as' => 'exchangedetails', 'uses' => 'ExchangeController@exchangedetails']);
//Route::get('exchange/coinsprice/{exchange_id}', ['as' => 'exchangecoinsprice', 'uses' => 'ExchangeController@coinsprice']);

Route::get('exchange/updatestatus/{exchange_id}/{exchange_status}', ['as' => 'exchangeupdatestatus', 'uses' => 'ExchangeController@exchangeupdatestatus']);
Route::get('exchange/currencyconvert/{currency}', ['as' => 'currencyconvert', 'uses' => 'ExchangeDataconverterController@currencyconvert']);
Route::get('exchange/currencyconvert01/{currency}', ['as' => 'currencyconvert01', 'uses' => 'ExchangeDataconverterController@currencyconvert01']);
Route::get('exchange/alldata', ['as' => 'exchagesalldatanew', 'uses' => 'ExchangeController@exchagesalldatanew']);
Route::get('exchange/converttype/{exchange_id}/{converttype}', ['as' => 'converttype', 'uses' => 'ExchangeController@converttype']);
Route::any('addexchange', ['as' => 'addexchange', 'uses' => 'ExchangeController@addexchange']);
Route::any('insertmarket', ['as' => 'insertmarket', 'uses' => 'ExchangeMarketController@insertmarket']);
Route::any('addeditmarket', ['as' => 'addeditmarket', 'uses' => 'ExchangeController@addeditmarket']);
Route::any('addeditimpurl', ['as' => 'addeditimpurl', 'uses' => 'ExchangeController@addeditimpurl']);
Route::any('updatemarketnote', ['as' => 'updatemarketnote', 'uses' => 'ExchangeController@updatemarketnote']);
Route::any('coindata', ['as' => 'updatecoindata', 'uses' => 'CoindataController@coindata']);
Route::any('coindata/{exchange_id}', ['as' => 'coindata', 'uses' => 'CoindataController@coindata']);
Route::any('pairsymbolfrm', ['as' => 'pairsymbolfrm', 'uses' => 'ExchangeController@pairsymbolfrm']);

//pairprice//Route::get('coinsprices', ['as' => 'coinsprices', 'uses' => 'ExchangeController@coinsprices']);
//pairprice//Route::get('coinsprices/{api}', ['as' => 'coinspricesapi', 'uses' => 'ExchangeController@coinsprices']);

Route::get('allcoindata', ['as' => 'currencyconvert0441', 'uses' => 'ExchangeDataconverterController@currencyconvert']);

/*************************/
Route::get('coinprice', ['as' => 'coinprice', 'uses' => 'ExchangeController@coinprice']);
Route::get('pairprice', ['as' => 'pairprice', 'uses' => 'ExchangeController@pairprice']);//coinsprices
Route::get('allapiurl', ['as' => 'allapiurl', 'uses' => 'ExchangeController@allapiurl']);

Route::get('testctrl', ['as' => 'testctrl', 'uses' => 'TestctrlController@testctrl']);
Route::get('exchange-market-volume', ['as' => 'exchangemarketvolume', 'uses' => 'ExchangeController@exchangemarketvolume']);

//exchange/
Route::get('exchangeconvert/{id}/{convert_type}/{data_update_type}', ['as' => 'exchangeconvert', 'uses' => 'ExchangeController@exchangeconvert']);
Route::any('editcoin/{id}/{name}/{updte}', ['as' => 'editcoin', 'uses' => 'ExchangeController@editcoin']);
Route::get('coinstatus/{id}', ['as' => 'coinstatus', 'uses' => 'ExchangeController@coinstatus']);

Route::get('currencyconverter', ['as' => 'currencyconverter', 'uses' => 'ExchangeController@currencyconverter']);
Route::get('exchange-volume-accuracy', ['as' => 'exchangevolumeaccuracy', 'uses' => 'ExchangeController@exchangevolumeaccuracy']);

Route::get('currencygroup', ['as' => 'currencygroup', 'uses' => 'ExchangeController@currencygroup']);
Route::get('trademarket', ['as' => 'trademarket', 'uses' => 'UpticksController@trademarket']);


?>
